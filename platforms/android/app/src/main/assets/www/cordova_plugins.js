cordova.define('cordova/plugin_list', function(require, exports, module) {
  module.exports = [
    {
      "id": "cordova-plugin-vidvekyc.VIDVEKYCPlugin",
      "file": "plugins/cordova-plugin-vidvekyc/www/VIDVEKYCPlugin.js",
      "pluginId": "cordova-plugin-vidvekyc",
      "clobbers": [
        "VIDVEKYCPlugin"
      ]
    }
  ];
  module.exports.metadata = {
    "cordova-plugin-vidvekyc": "1.1.0-alpha09"
  };
});