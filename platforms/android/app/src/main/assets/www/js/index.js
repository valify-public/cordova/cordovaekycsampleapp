/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

// Wait for the deviceready event before using any of Cordova's device APIs.
// See https://cordova.apache.org/docs/en/latest/cordova/events/events.html#deviceready
document.getElementById("button").addEventListener("click",startEKYC);

const baseUrl="Place_your_baseurl_here";
const token="Place_token_here" ;
const bundleKey="Place_your_bundleKey_her";
const language="en";
const color="#FF018786";
const headers = {};
const documentVerification = false;
const dataValidation = false;
const returnDataValidationError = false;
const reviewData = true;
const enableSmile = true;
const enableLookLeft = true;
const enableLookRight = true;
const enableCloseEyes = true;
const livenessNumberOfInstructions = "4";
const livenessNumberOfFailedTrials = "3";
const livenessTimePerAction = "10";
const enableVoiceover = true;
const showErrorMessage = true;

const params = {
  access_token: token,
  base_url: baseUrl,
  bundle_key: bundleKey,
  language: language,
  primary_color: color,
  document_verification: documentVerification,
  data_validation: dataValidation,
  return_data_validation_error: returnDataValidationError,
  review_data: reviewData,
  enable_smile: enableSmile,
  enable_look_left: enableLookLeft,
  enable_look_right: enableLookRight,
  enable_close_eyes: enableCloseEyes,
  liveness_number_of_instructions: livenessNumberOfInstructions,
  liveness_number_of_failed_trials: livenessNumberOfFailedTrials,
  liveness_time_per_action: livenessTimePerAction,
  enable_voiceover: enableVoiceover,
  show_error_message: showErrorMessage,
};

function startEKYC(){


window.VIDVEKYCPlugin.startEKYC(
        params, headers,
        (result) => {
          console.log('valify success result : ', result);
          const s = result.toString();
          const json = JSON.parse(s);

          // do your own logic
        },
        (error) => {
          console.log('error in valify', error);
          const s = error.toString();
          const json = JSON.parse(s);
          if (json.nameValuePairs.errorCode!=0) {
            // user exits the SDK due to an error occurred
            alert(json.nameValuePairs.errorCode);
          } else {
            // user exits the SDK without error
            alert("user exits the SDK without error");
          }
          // do your own logic
        });

}
