package cordova_plugin_vidvekyc;

import android.content.Intent;

import com.google.gson.Gson;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class VIDVEKYCPlugin extends CordovaPlugin {
    public static CallbackContext callbackContext;

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("startEKYC")) {
            this.startEKYC(args, callbackContext);
            return true;
        }
        return false;
    }

    private void startEKYC(JSONArray args, CallbackContext callbackContext) throws JSONException {
        this.callbackContext = callbackContext;

        JSONObject jsonObject = args.getJSONObject(0);
        Intent intent = new Intent("cordova.plugin.vidvekyc.VIDVEKYCActivity");
        intent.putExtra("base_url", jsonObject.getString("base_url"));
        intent.putExtra("access_token", jsonObject.getString("access_token"));
        intent.putExtra("bundle_key", jsonObject.getString("bundle_key"));
        intent.putExtra("language", jsonObject.getString("language"));

        if (jsonObject.has("primary_color"))
            intent.putExtra("primary_color", jsonObject.getString("primary_color"));
        if (!args.isNull(1))
            intent.putExtra("headers", new Gson().fromJson(String.valueOf(args.getJSONObject(1)), HashMap.class));
//        if (jsonObject.has("ssl_certificate"))
//            intent.putExtra("ssl_certificate", jsonObject.get("ssl_certificate"));

        if (jsonObject.has("document_verification"))
            intent.putExtra("document_verification", jsonObject.getBoolean("document_verification"));
        if (jsonObject.has("data_validation"))
            intent.putExtra("data_validation", jsonObject.getBoolean("data_validation"));
        if (jsonObject.has("return_data_validation_error"))
            intent.putExtra("return_data_validation_error", jsonObject.getBoolean("return_data_validation_error"));
        if (jsonObject.has("review_data"))
            intent.putExtra("review_data", jsonObject.getBoolean("review_data"));

        if (jsonObject.has("enable_smile"))
            intent.putExtra("enable_smile", jsonObject.getBoolean("enable_smile"));
        if (jsonObject.has("enable_look_left"))
            intent.putExtra("enable_look_left", jsonObject.getBoolean("enable_look_left"));
        if (jsonObject.has("enable_look_right"))
            intent.putExtra("enable_look_right", jsonObject.getBoolean("enable_look_right"));
        if (jsonObject.has("enable_close_eyes"))
            intent.putExtra("enable_close_eyes", jsonObject.getBoolean("enable_close_eyes"));
        if (jsonObject.has("liveness_number_of_failed_trials"))
            intent.putExtra("liveness_number_of_failed_trials", jsonObject.getInt("liveness_number_of_failed_trials"));
        if (jsonObject.has("liveness_number_of_instructions"))
            intent.putExtra("liveness_number_of_instructions", jsonObject.getInt("liveness_number_of_instructions"));
        if (jsonObject.has("liveness_time_per_action"))
            intent.putExtra("liveness_time_per_action", jsonObject.getInt("liveness_time_per_action"));
        if (jsonObject.has("enable_voiceover"))
            intent.putExtra("enable_voiceover", jsonObject.getBoolean("enable_voiceover"));
        if (jsonObject.has("show_error_message"))
            intent.putExtra("show_error_message", jsonObject.getBoolean("show_error_message"));

        cordova.startActivityForResult(this, intent, 1);
    }
}
