package cordova.plugin.vidvekyc;

import android.app.Activity;
import android.graphics.Color;
import com.google.gson.Gson;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import me.vidv.vidvekycsdk.sdk.BuilderError;
import me.vidv.vidvekycsdk.sdk.ServiceFailure;
import me.vidv.vidvekycsdk.sdk.Success;
import me.vidv.vidvekycsdk.sdk.UserExit;
import me.vidv.vidvekycsdk.sdk.CapturedImages;
import me.vidv.vidvekycsdk.sdk.LivenessImage;
import me.vidv.vidvekycsdk.sdk.OCRImage;
import me.vidv.vidvekycsdk.sdk.VIDVEKYCBuilder;
import me.vidv.vidvekycsdk.sdk.VIDVEKYCLanguage;
import me.vidv.vidvekycsdk.sdk.VIDVEKYCListener;
import me.vidv.vidvekycsdk.sdk.VIDVEKYCResponse;
import me.vidv.vidvlivenesssdk.sdk.VIDVLivenessConfig;
import me.vidv.vidvocrsdk.sdk.VIDVOCRConfig;

public class VIDVEKYCActivity extends Activity {

    private CallbackContext callbackContext;

    @Override
    protected void onStart() {
        super.onStart();
        callbackContext = cordova_plugin_vidvekyc.VIDVEKYCPlugin.callbackContext;
        startService();
    }

    private void startService() {
        VIDVEKYCBuilder builder = new VIDVEKYCBuilder();

        if (getIntent().hasExtra("base_url")) {
            builder.setBaseUrl(getIntent().getExtras().getString("base_url"));
        }
        if (getIntent().hasExtra("access_token")) {
            builder.setAccessToken(getIntent().getExtras().getString("access_token"));
        }
        if (getIntent().hasExtra("bundle_key")) {
            builder.setBundleKey(getIntent().getExtras().getString("bundle_key"));
        }
        if (getIntent().hasExtra("language")) {
            builder.setLanguage(getIntent().getExtras().getString("language")); 
        }
        if (getIntent().hasExtra("headers")) {
            builder.setHeaders((HashMap<String, String>) getIntent().getExtras().get("headers"));
        }
        if (getIntent().hasExtra("ssl_certificate")) {
            //    builder.setSSLCertificate(getIntent().getExtras().get("ssl_certificate"));
        }
        if (getIntent().hasExtra("primary_color") && !getIntent().getExtras().getString("primary_color").equals("")) {
                builder.setPrimaryColor(Color.parseColor(getIntent().getExtras().getString("primary_color")));
        }

        VIDVOCRConfig.Builder ocrConfig = new VIDVOCRConfig.Builder();
        if (getIntent().hasExtra("document_verification")) {
            ocrConfig.setDocumentVerification(getIntent().getExtras().getBoolean("document_verification"));
        }
        if (getIntent().hasExtra("data_validation")) {
            ocrConfig.setDataValidation(getIntent().getExtras().getBoolean("data_validation"));
        }
        if (getIntent().hasExtra("return_data_validation_error")) {
            ocrConfig.setReturnValidationError(getIntent().getExtras().getBoolean("return_data_validation_error"));
        }
        if (getIntent().hasExtra("review_data")) {
            ocrConfig.setReviewData(getIntent().getExtras().getBoolean("review_data"));
        }
        builder.setOCRConfigurations(ocrConfig);

        VIDVLivenessConfig.Builder livenessConfig = new VIDVLivenessConfig.Builder();
        if (getIntent().hasExtra("enable_smile")) {
            if (!getIntent().getExtras().getBoolean("enable_smile")) livenessConfig.withoutSmile();
        }
        if (getIntent().hasExtra("enable_look_left")) {
            if (!getIntent().getExtras().getBoolean("enable_look_left"))
                livenessConfig.withoutLookLeft();
        }
        if (getIntent().hasExtra("enable_look_right")) {
            if (!getIntent().getExtras().getBoolean("enable_look_right"))
                livenessConfig.withoutLookRight();
        }
        if (getIntent().hasExtra("enable_close_eyes")) {
            if (!getIntent().getExtras().getBoolean("enable_close_eyes"))
                livenessConfig.withoutCloseEyes();
        }
        if (getIntent().hasExtra("liveness_number_of_instructions")) {
            livenessConfig.setNumberOfInstructions(getIntent().getExtras().getInt("liveness_number_of_instructions"));
        }
        if (getIntent().hasExtra("liveness_number_of_failed_trials")) {
            livenessConfig.setFailTrials(getIntent().getExtras().getInt("liveness_number_of_failed_trials"));
        }
        if (getIntent().hasExtra("liveness_time_per_action")) {
            livenessConfig.setInstructionTimer(getIntent().getExtras().getInt("liveness_time_per_action"));
        }
        if (getIntent().hasExtra("enable_voiceover")) {
            if (!getIntent().getExtras().getBoolean("enable_voiceover"))
                livenessConfig.withoutVoiceOver();
        }
        if (getIntent().hasExtra("show_error_message")) {
            livenessConfig.showErrorDialogs(getIntent().getExtras().getBoolean("show_error_message"));
        }
        builder.setLivenessConfigurations(livenessConfig);

        builder.start(cordova.plugin.vidvekyc.VIDVEKYCActivity.this, new VIDVEKYCListener() {
            @Override
            public void onEKYCResult(VIDVEKYCResponse response) {
                if (response instanceof Success) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("status", "finalResult");
                        jsonObject.put("ekycResult", ((Success) response).getData());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String jsonInString = new Gson().toJson(jsonObject);

                    PluginResult resultado = new PluginResult(PluginResult.Status.OK, jsonInString);

                    resultado.setKeepCallback(true);
                    callbackContext.sendPluginResult(resultado);

                    finish();// Exit of this activity !
                } else if (response instanceof BuilderError) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("errorCode", ((BuilderError) response).getCode());
                        jsonObject.put("errorMessage", ((BuilderError) response).getMessage());
                       
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String jsonInString = new Gson().toJson(jsonObject);

                    PluginResult resultado = new PluginResult(PluginResult.Status.ERROR, jsonInString);

                    resultado.setKeepCallback(true);
                    callbackContext.sendPluginResult(resultado);
                    finish();// Exit of this activity !
                } else if (response instanceof ServiceFailure) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("errorCode", ((ServiceFailure) response).getCode());
                        jsonObject.put("errorMessage", ((ServiceFailure) response).getMessage());
                        jsonObject.put("ekycResult", ((ServiceFailure) response).getData());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String jsonInString = new Gson().toJson(jsonObject);

                    PluginResult resultado = new PluginResult(PluginResult.Status.ERROR, jsonInString);

                    resultado.setKeepCallback(true);
                    callbackContext.sendPluginResult(resultado);
                    finish();// Exit of this activity !
                } else if (response instanceof UserExit) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("errorCode", 0);
                        jsonObject.put("step", ((UserExit) response).getStep());
                        jsonObject.put("ekycResult", ((UserExit) response).getData());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String jsonInString = new Gson().toJson(jsonObject);

                    PluginResult resultado = new PluginResult(PluginResult.Status.ERROR, jsonInString);

                    resultado.setKeepCallback(true);
                    callbackContext.sendPluginResult(resultado);

                    finish();// Exit of this activity !
                } else if (response instanceof CapturedImages) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("status", "onGoingData");
                        if (((CapturedImages) response).getSessionImages() instanceof OCRImage)
                        jsonObject.put("ocrImage", ((CapturedImages) response).getSessionImages());
                        else if (((CapturedImages) response).getSessionImages() instanceof LivenessImage)
                            jsonObject.put("livenessImage", ((CapturedImages) response).getSessionImages());                 
                        } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String jsonInString = new Gson().toJson(jsonObject);

                    PluginResult resultado = new PluginResult(PluginResult.Status.OK, jsonInString);

                    resultado.setKeepCallback(true);
                    callbackContext.sendPluginResult(resultado);
                }
            }
        });
    }
}